TF_ENABLE_ONEDNN_OPTS=0
from flask import Flask, render_template, request, jsonify
import tensorflow as tf
from tensorflow.keras.preprocessing import image
import numpy as np
import os

app = Flask(__name__)

# Load the pre-trained model
model = tf.keras.models.load_model('C:\\Users\\mateu\\Documents\\projekt-semestralny\\best_model.h5')

# Function to predict the class of an image along with probability distribution
def predict_image(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array = np.expand_dims(img_array, axis=0)
    img_array /= 255.0  # Scale pixels to the range [0, 1]

    prediction = model.predict(img_array)
    probabilities = prediction.tolist()[0]  # Convert numpy array to a list
    predicted_class = np.argmax(prediction)

    return predicted_class, probabilities

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # Check if a file was uploaded
        if 'file' not in request.files:
            return jsonify({'error': 'No file part'})

        file = request.files['file']

        # Check if the file is not empty
        if file.filename == '':
            return jsonify({'error': 'No selected file'})

        # Check if it's a valid image file
        allowed_extensions = {'png', 'jpg', 'jpeg', 'gif'}
        if '.' not in file.filename or file.filename.rsplit('.', 1)[1].lower() not in allowed_extensions:
            return jsonify({'error': 'Invalid file format'})

        # Save the file to a temporary location on the server
        img_path = 'C:/Users/mateu/Documents/projekt-semestralny/uploads/temp_image.jpg'
        file.save(img_path)

        # Predict the image class and get probabilities
        predicted_class, probabilities = predict_image(img_path)

        # Map the class index to a name
        class_mapping = {0: 'Healthy', 1: 'Powdery', 2: 'Rust'}
        predicted_class_name = class_mapping[predicted_class]
        
        return jsonify({'class': predicted_class_name, 'probabilities': probabilities})
        
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)
